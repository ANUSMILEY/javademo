package com.login.dao;

import com.login.exception.Raiseexception;
import com.login.model.user;

public interface Logindao {
	public abstract user create(user user);//for saving the data
     public abstract user readByuserIdandPassword(user user) throws Raiseexception;//fetching data
     public abstract user update(user user);  //modify 
     public abstract user delete(int userId);//just provide id to delete
 
}
