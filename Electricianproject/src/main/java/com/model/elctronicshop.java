package com.model;

public class elctronicshop {
	private String shopName;
	private String shopAddress;
	private int gstnum;
	private Electrician[] electrician;

	public elctronicshop() {
		super();
		// TODO Auto-generated constructor stub
	}

	public elctronicshop(String shopName, String shopAddress, int gstnum, Electrician[] electrician) {
		super();
		this.shopName = shopName;
		this.shopAddress = shopAddress;
		this.gstnum = gstnum;
		this.electrician = electrician;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public int getGstnum() {
		return gstnum;
	}

	public void setGstnum(int gstnum) {
		this.gstnum = gstnum;
	}

	public Electrician[] getElectrician() {
		return electrician;
	}

	public void setElectrician(Electrician[] electrician) {
		this.electrician = electrician;
	}

}
