package com.model;

public class Electrician {
	private int contactnum;
	private String elecName;
	private int age;

	public Electrician() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Electrician(int contactnum, String elecName, int age) {
		super();
		this.contactnum = contactnum;
		this.elecName = elecName;
		this.age = age;
	}

	public int getContactnum() {
		return contactnum;
	}

	public void setContactnum(int contactnum) {
		this.contactnum = contactnum;
	}

	public String getElecName() {
		return elecName;
	}

	public void setElecName(String elecName) {
		this.elecName = elecName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
