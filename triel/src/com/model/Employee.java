package com.model;

public class Employee {
	private int Empid;
	private String EmpName;
	private int salary;

	public Employee() {
		super();

	}

	public Employee(int empid, String empName, int salary) {
		super();
		Empid = empid;
		EmpName = empName;
		this.salary = salary;
	}

	public int getEmpno() {
		return Empid;
	}

	public void setEmpno(int empno) {
		Empid = empno;
	}

	public String getEmpName() {
		return EmpName;
	}

	public void setEmpName(String empName) {
		EmpName = empName;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
}

	