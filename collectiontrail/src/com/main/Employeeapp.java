package com.main;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.model.employee;

public class Employeeapp {

	public static void main(String[] args) {
		employee employee1 = new employee(18,"anu",2020.20f);
		employee employee2 = new employee(20,"tej",3030.30f);
		employee employee3 = new employee(19,"sri",4040.40f);
		
		//<INTERFACENAME> VAR = NEW <INTERFACEIMPL>
		Set set = new HashSet();  //no size limit==no orderand no duplicates
		set.add(employee1);
		set.add(employee2);
		set.add(employee3);

		employee employee4 = new employee(23,"shi", 5050.90f);
		set.add(employee4);
		set.add(employee1);  //duplicate
		System.out.println("ow many:"+set.size());
		
		//iterate
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			employee object = (employee) iterator.next();
			
			System.out.println(object.getEmpno());
			System.out.println(object.getEmpname());
			System.err.println(object.getSalary());
			
			
		}
			
		}
}
		
	
	
	
	
