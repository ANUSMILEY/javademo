package com.model;

public class employee {

	private int empno;
	private String empname;
	private float salary;

	public employee() {
		super();

	}

	public employee(int empno, String empname, float salary) {
		super();
		this.empno = empno;
		this.empname = empname;
		this.salary = salary;
	}

	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
