package com.main;

import com.exception.positivenumexception;
import com.service.calculator;
import com.service.calculatorimpl;

public class calculatorapp {

	public static void main(String[] args) {
		calculator calculator = new calculatorimpl();
		System.out.println("addition of two nums:" + calculator.add(2, 3));
		try {
			System.out.println("substraction of two nums:" + calculator.sub(2,-3));
		} catch (positivenumexception Pe) {
			System.err.println(Pe.getMessage());
		}
		System.out.println("multiplication of two nums:" + calculator.mul(2,-3));
		try {
			System.out.println("division of two nums:" + calculator.div(10, 0));
		} catch (positivenumexception pne) {
			System.err.println(pne.getMessage());
			}
		System.out.println("exit");
		calculator = null;

	}

}
