package com.service;

import com.exception.positivenumexception;

public interface calculator {
	public abstract int add(int num1,int num2);
	public abstract int sub(int num1,int num2) throws positivenumexception;
	public abstract int mul(int num1,int num2);
	public abstract int div(int num1,int num2) throws positivenumexception;
	

}
