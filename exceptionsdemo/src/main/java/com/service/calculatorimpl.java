package com.service;

import com.exception.positivenumexception;

public class calculatorimpl implements calculator {

	@Override
	public int add(int num1, int num2) {

		return num1 + num2;
	}

	// allow only positive numbers
	int temp = 0;

	public int sub(int num1, int num2) {

		if (num1 > 0 && num2 > 0) {

			temp = num1 - num2;
		} else {
			try {
				throw new positivenumexception();// throw used to raise the exceptiom
			} catch (positivenumexception pe) {

				System.err.println(pe.getMessage());

			}

		}
		return temp;
	}

	@Override
	public int mul(int num1, int num2) {

		return num1 * num2;
	}

	@Override

	public int div(int num1, int num2) throws positivenumexception {
		int temp = 0;
		if (num1 > 0 && num2 > 0) {

			temp = num1 / num2;
		} 
		else
		{
              throw new positivenumexception();
		}

		return temp;
	}

}
